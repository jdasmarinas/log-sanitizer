#!/usr/bin/env perl

use strict;
use warnings;
use 5.012;
use Digest::MD5 qw(md5_hex);
no warnings 'experimental';

sub parse_arguments {
  my @args = @_;
  my %context = (
    "network" => 0,
    "username" => 0,
    "fullpath" => 0,
    "ldap" => 0,
    "replace" => 0,
  );
  foreach (@args) {
    given ($_) {
      when (/\-n/) { $context{"network"} = 1; }
      when (/\-u/) { $context{"username"} = 1; }
      when (/\-p/) { $context{"fullpath"} = 1; }
      when (/\-l/) { $context{"ldap"} = 1; }
      when (/\-r/) { $context{"replace"} = 1; }
      default      { $context{"log_file"} = "$_"; last}
    }
  }
  return %context;
}


sub sanitize_network {
  my $log_file = shift;
  my $replace = shift;

  my %matches = (
    'xx.xx.xx.xx' => '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}',
    '\'xx.xx.xx:xx\'' => '\'[\w\-\.]+:\d+\'',
  );

  my %unique_network_data;
  my @network_data = (
    $log_file =~ m/"host":"(.*?)"/igs,
  );

  @unique_network_data{@network_data} = ();

  # Sanitize generic matches
  my $replacement;
  foreach my $key (keys %matches) {
    my $filter = $matches{$key};
    $replacement = $key;
    $replacement = 'sanitized-ip-' . md5_hex($key) if $replace == 1;
    $log_file =~ s/$filter/$replacement/ig;
  }

  # Sanitize hostnae
  $replacement = 'xx.xx.xx';
  foreach (keys %unique_network_data) {
    next if /^localhost$/;
    next if not $_;
    $replacement = 'sanitized-host-' . md5_hex($_) if $replace == 1;
    $log_file =~ s/$_/$replacement/ig;
  }

  return $log_file;
}

sub sanitize_username {
  my $log_file = shift;
  my $replace = shift;

  my %matches = (
    'xx@xx.xx' => '[\w\-\.]+@[\w\-\.]+',
  );

  my %unique_usernames;
  my @usernames = (
    $log_file =~ m/"username":"(.*?)"/igs,
    $log_file =~ m/"author_name":"(.*?)"/igs,
    $log_file =~ m/"meta.user":"(.*?)"/igs,
  );

  # Sanitize generic email address
  my $replacement;
  foreach my $key (keys %matches) {
    my $filter = $matches{$key};
    $replacement = $key;
    $replacement = 'sanitized-email-' . md5_hex($key) if $replace == 1;
    $log_file =~ s/$filter/$replacement/ig;
  }

  @unique_usernames{@usernames} = ();

  $replacement = 'xx';
  foreach (keys %unique_usernames) {
    next if /^root$/;
    next if not $_;
    $replacement = 'sanitized-username-' . md5_hex($_) if $replace == 1;
    $log_file =~ s/$_/$replacement/g;
  }

  return $log_file;
}

sub sanitize_ldap {
  my $log_file = shift;
  my $replace = shift;

  my %unique_ldap_data;
  my @ldap_data = (
    $log_file =~ m/user (\w+),/igs,
    $log_file =~ m/GitLab user "(.+?)"/igs,
    $log_file =~ m/CN=([\w\s]+),/igs,
    $log_file =~ m/givenname: (\w+)/igs,
    $log_file =~ m/samaccountname: (\w+)/igs,
    $log_file =~ m/sn: (\w+)/igs,
  );

  @unique_ldap_data{@ldap_data} = ();

  my %matches = (
    'cn=xx,dc=xx' => 'cn=.+?dc=\w+',
  );

  my $replacement = 'xx';
  foreach (keys %unique_ldap_data) {
    next if not $_;
    $replacement = 'sanitized-ldap-' . md5_hex($_) if $replace == 1;
    $log_file =~ s/$_/$replacement/g;
  }

  # Sanitize generic LDAP data
  foreach my $key (keys %matches) {
    my $filter = $matches{$key};
    $replacement = $key;
    $replacement = 'sanitized-ldap-' . md5_hex($key) if $replace == 1;
    $log_file =~ s/$filter/$replacement/ig;
  }

  return $log_file;
}


sub sanitize_fullpath {
  my $log_file = shift;
  my $replace = shift;

  my %unique_path;
  my @paths = (
    $log_file =~ m/"meta.project":"(.*?)"/igs,
    $log_file =~ m/"grpc.request.glProjectPath":"(.*?)"/igs,
  ); 

  @unique_path{@paths} = ();

  my $replacement = 'xx/xx';
  foreach (keys %unique_path) {
    next if not $_;
    $replacement = 'sanitized-path-' . md5_hex($_) if $replace == 1;
    $log_file =~ s|$_|$replacement|ig;
  }

  return $log_file;
}

my %context = &parse_arguments (@ARGV);
my $file = $context{"log_file"};

open(my $fh, '<', $file) || die "Can't open file, $!";
my $log_file = do { local($/); <$fh> };
close ($fh);

$log_file = &sanitize_network($log_file, $context{"replace"})       if $context{"network"}  == 1;
$log_file = &sanitize_username($log_file, $context{"replace"}) if $context{"username"} == 1;
$log_file = &sanitize_ldap($log_file, $context{"replace"})     if $context{"ldap"}     == 1;
$log_file = &sanitize_fullpath($log_file, $context{"replace"}) if $context{"fullpath"} == 1;
print $log_file;
